const DSSV = "DSSV";
var dssv = [];

var dataJson = localStorage.getItem(DSSV);
if (dataJson) {
  var dataRaw = JSON.parse(dataJson);
  dssv = dataRaw.map(function (item) {
    return new SinhVien_model(
      item.id,
      item.name,
      item.email,
      item.password,
      item.scorePhys,
      item.scoreMath,
      item.scoreChem
    );
  });

  renderStudentInfo(dssv);
}

function saveLocalStorage() {
  var dssvJson = JSON.stringify(dssv);
  localStorage.setItem(DSSV, dssvJson);
}

function resetForm() {
  document.getElementById("formQLSV").reset();
  document.getElementById("txtMaSV").disabled = false;
  document.getElementById("btnthemSv").disabled = false;
}

function themSv() {
  var newSv = getStudentInfo();

  // validation
  var isValid = true;

  isValid =
    isValid & requiredCheck(newSv.id, "spanMaSV") &&
    cloneCheck(newSv.id, dssv, "spanMaSV");

  isValid &= requiredCheck(newSv.name, "spanTenSV");
  isValid &=
    requiredCheck(newSv.email, "spanEmailSV") &&
    emailValid(newSv.email, "spanEmailSV");
  isValid &= requiredCheck(newSv.password, "spanMatKhau");
  isValid &= requiredCheck(newSv.scoreChem, "spanHoa");
  isValid &= requiredCheck(newSv.scorePhys, "spanLy");
  isValid &= requiredCheck(newSv.scoreMath, "spanToan");

  if (!isValid) return;

  dssv.push(newSv);
  saveLocalStorage();
  renderStudentInfo(dssv);
  resetForm();
}

function xoaSv(idSv) {
  var index = dssv.findIndex(function (sv) {
    return sv.id == idSv;
  });

  if (index == -1) return;
  dssv.splice(index, 1);
  renderStudentInfo(dssv);
  saveLocalStorage();
}

function suaSv(idSv) {
  var index = dssv.findIndex(function (sv) {
    return sv.id == idSv;
  });

  if (index == -1) return;

  var svEdit = dssv[index];
  displayInfo(svEdit);
  document.getElementById("txtMaSV").disabled = true;
  document.getElementById("btnthemSv").disabled = true;
}

function update() {
  document.getElementById("txtMaSV").disabled = false;
  document.getElementById("btnthemSv").disabled = false;

  var svEdit = getStudentInfo();

  var index = dssv.findIndex(function (sv) {
    return sv.ma == svEdit.ma;
  });

  if (index == -1) return;

  dssv[index] = svEdit;
  saveLocalStorage();
  renderStudentInfo(dssv);
  resetForm();

  // console.log("svEdit: ", svEdit);
}
