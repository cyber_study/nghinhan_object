// OBJECT
function SinhVien_model(
  _id,
  _name,
  _email,
  _password,
  _scoreMath,
  _scorePhys,
  _scoreChem
) {
  // properties
  this.id = _id;
  this.name = _name;
  this.email = _email;
  this.password = _password;
  this.scoreMath = _scoreMath;
  this.scorePhys = _scorePhys;
  this.scoreChem = _scoreChem;
  // method
  this.averageScore = function () {
    return (
      (this.scoreMath * 1 + this.scorePhys * 1 + this.scoreChem * 1) /
      3
    ).toFixed(1);
  };
}

// VALIDATION MODULE
function requiredCheck(value, idError) {
  if (value.length == 0) {
    document.getElementById(idError).innerText = "Không được để rỗng";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function cloneCheck(idSv, listSv, idError) {
  var index = listSv.findIndex(function (sv) {
    return sv.id == idSv;
  });

  if (index == -1) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Mã sinh viên đã tồn tại";
    return false;
  }
}

function emailValid(value, idError) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(value);
  if (isEmail) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Wrong email format!";
    return false;
  }
}
