function getStudentInfo() {
  var idSv = document.getElementById("txtMaSV").value.trim();
  var nameSv = document.getElementById("txtTenSV").value.trim();
  var emailSv = document.getElementById("txtEmail").value.trim();
  var password = document.getElementById("txtPass").value.trim();
  var scoreMath = document.getElementById("txtDiemToan").value.trim();
  var scorePhys = document.getElementById("txtDiemLy").value.trim();
  var scoreChem = document.getElementById("txtDiemHoa").value.trim();

  var sv = new SinhVien_model(
    idSv,
    nameSv,
    emailSv,
    password,
    scoreMath,
    scorePhys,
    scoreChem
  );
  return sv;
}

function renderStudentInfo(list) {
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var currentSv = list[i];
    var contentTr = `
    <tr>    
      <td>${currentSv.id}</td>
      <td>${currentSv.name}</td>
      <td>${currentSv.email}</td>
      <td>${currentSv.averageScore()}</td>
      <td>
        <button class="btn btn-primary" onclick="suaSv('${
          currentSv.id
        }')">Sửa</button>
        <button class="btn btn-danger" onclick="xoaSv(${
          currentSv.id
        })">Xóa</button>
      </td>                     
    </tr>
    `;
    contentHTML += contentTr;
  }
  // In thông tin
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function displayInfo(sv) {
  document.getElementById("txtMaSV").value = sv.id;
  document.getElementById("txtTenSV").value = sv.name;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.password;
  document.getElementById("txtDiemLy").value = sv.scorePhys;
  document.getElementById("txtDiemToan").value = sv.scoreMath;
  document.getElementById("txtDiemHoa").value = sv.scoreChem;
}
